﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SurvivalWriter
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        int timeLimit_M;
        int timeLimit_S;
        int Fallonfall;
        int CaBoom;
        int CaBoomCountdown;

        private void 分ToolStripMenuItem_Click(object sender, EventArgs e) //5分
        {
            toolStripStatusLabel2.Text = "5";
            toolStripStatusLabel4.Text = "0";
            toolStripButton4.Enabled = true;
        }

        private void 分ToolStripMenuItem1_Click(object sender, EventArgs e) //10m
        {
            toolStripStatusLabel2.Text = "10";
            toolStripStatusLabel4.Text = "0";
            toolStripButton4.Enabled = true;
        }

        private void 分ToolStripMenuItem2_Click(object sender, EventArgs e) //30m
        {
            toolStripStatusLabel2.Text = "30";
            toolStripStatusLabel4.Text = "0";
            toolStripButton4.Enabled = true;
        }

        private void 分ToolStripMenuItem3_Click(object sender, EventArgs e) //60m
        {
            toolStripStatusLabel2.Text = "60";
            toolStripStatusLabel4.Text = "0";
            toolStripButton4.Enabled = true;
        }

        private void richTextBox1_KeyUp(object sender, KeyEventArgs e)
        {
            if (!int.TryParse(toolStripStatusLabel8.Text, out CaBoom))
            {
                CaBoom = 1;
            }
            CaBoomCountdown = 0;
            toolStripStatusLabel10.Text = "(消滅開始)";
            timer2.Start();
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
            timer2.Stop();
            toolStripStatusLabel8.Text = "7";
            toolStripStatusLabel10.Text = "(停止中)";
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            int remn;
            CaBoomCountdown++;
            remn = CaBoom - CaBoomCountdown;
            toolStripStatusLabel8.Text = remn.ToString();

            if (CaBoom == CaBoomCountdown)
            {
                timer1.Stop();
                timer2.Stop();
                int finaltxt = richTextBox1.TextLength;
                MessageBox.Show("GAME OVER\n打ち込んだ文字数 " + finaltxt.ToString() + "\n\nお気の毒ですが文章は全て消滅します", "33-4", MessageBoxButtons.OK, MessageBoxIcon.Error);
                richTextBox1.Clear();

                richTextBox1.Visible = false;
                toolStripStatusLabel2.Text = ".";
                toolStripStatusLabel4.Text = ".";
                toolStripStatusLabel8.Text = "7";
                toolStripStatusLabel10.Text = "(停止中)";
                toolStripButton1.Enabled = true;
                toolStripButton2.Enabled = false;
                toolStripButton4.Enabled = false;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            int TimeLimit;

            if (timeLimit_S == Fallonfall)
            {
                if (timeLimit_M == 0)
                {
                    timer1.Stop();
                    timer2.Enabled = false;
                    MessageBox.Show("おめでとうございます！完走しました", "Mission Completed", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    richTextBox1.SelectAll();
                    richTextBox1.Copy();

                    TimeLimit = 0;
                    toolStripStatusLabel4.Text = TimeLimit.ToString();
                    toolStripStatusLabel2.Text = TimeLimit.ToString();
                    Standard peacefull = new Standard();
                    peacefull.Show();
                    Hide();
                }
                else
                {
                    timeLimit_M--;
                    toolStripStatusLabel2.Text = timeLimit_M.ToString();

                    timeLimit_S = 59;
                    toolStripStatusLabel4.Text = timeLimit_S.ToString();
                    Fallonfall = 0;
                }
            }
            else
            {
                Fallonfall++;
                TimeLimit = timeLimit_S - Fallonfall;
                toolStripStatusLabel4.Text = TimeLimit.ToString();
            }
        }

        private void toolStripButton4_Click(object sender, EventArgs e) //START
        {
            if (int.TryParse(toolStripStatusLabel2.Text, out timeLimit_M) || int.TryParse(toolStripStatusLabel4.Text, out timeLimit_S))
            {
                richTextBox1.Visible = true;
                Fallonfall = 0;
                toolStripButton1.Enabled = false;
                toolStripButton4.Enabled = false;
                toolStripButton2.Enabled = true;
                timer1.Start();
            }
            else
            {
                MessageBox.Show("時間を指定してください", "おいおい", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void toolStripButton2_Click(object sender, EventArgs e) //リタイア
        {
            timer1.Stop();
            timer2.Stop();

            DialogResult aa_give_up;
            aa_give_up = MessageBox.Show("リタイアしますか？\nリタイアすると作成中の文章は全て削除されます", "愚か者め・・・", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
            if (aa_give_up == DialogResult.Yes)
            {
                richTextBox1.Clear();
                timeLimit_M = 0;
                timeLimit_S = 0;

                richTextBox1.Visible = false;
                toolStripStatusLabel2.Text = ".";
                toolStripStatusLabel4.Text = ".";
                toolStripButton1.Enabled = true;
                toolStripButton4.Enabled = false;
                toolStripButton2.Enabled = false;
            }
            else
            {
                timer1.Start();
                timer2.Start();
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            About verdlg = new About();
            verdlg.ShowDialog();
        }
    }
}
